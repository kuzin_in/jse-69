package ru.kuzin.tm.repository.dto;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kuzin.tm.dto.UserDTO;

@Repository
public interface IUserDtoRepository extends JpaRepository<UserDTO, String> {

    @Nullable
    UserDTO findByLogin(final String login);

}