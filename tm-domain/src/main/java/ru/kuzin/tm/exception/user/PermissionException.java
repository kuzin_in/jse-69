package ru.kuzin.tm.exception.user;

public final class PermissionException extends AbstractUserException {

    public PermissionException() {
        super("Error! You don't have permission to execute this command...");
    }

}