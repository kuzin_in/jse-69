package ru.kuzin.tm.listener.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.kuzin.tm.dto.request.ProjectStartByIndexRequest;
import ru.kuzin.tm.event.ConsoleEvent;
import ru.kuzin.tm.util.TerminalUtil;

@Component
public final class ProjectStartByIndexListener extends AbstractProjectListener {

    @NotNull
    private static final String NAME = "project-start-by-index";

    @NotNull
    private static final String DESCRIPTION = "Start project by index.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@projectStartByIndexListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(getToken());
        request.setIndex(index);
        projectEndpoint.startProjectByIndex(request);
    }

}