package ru.kuzin.tm.listener.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.kuzin.tm.dto.request.TaskChangeStatusByIndexRequest;
import ru.kuzin.tm.enumerated.Status;
import ru.kuzin.tm.event.ConsoleEvent;
import ru.kuzin.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class TaskChangeStatusByIndexListener extends AbstractTaskListener {

    @NotNull
    private static final String NAME = "task-change-status-by-index";

    @NotNull
    private static final String DESCRIPTION = "Change task status by index.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@taskChangeStatusByIndexListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(getToken());
        request.setIndex(index);
        request.setStatus(status);
        taskEndpoint.changeTaskStatusByIndex(request);
    }

}