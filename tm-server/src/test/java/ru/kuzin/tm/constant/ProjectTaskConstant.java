package ru.kuzin.tm.constant;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ProjectTaskConstant {

    int INIT_COUNT_TASKS = 5;

    @Nullable
    String NULLABLE_USER_ID = null;

    @Nullable
    String EMPTY_USER_ID = "";

    @Nullable
    String NULLABLE_PROJECT_ID = null;

    @NotNull
    String EMPTY_PROJECT_ID = "";

    @Nullable
    String NULLABLE_TASK_ID = null;

    @NotNull
    String EMPTY_TASK_ID = "";

}