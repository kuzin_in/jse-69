package ru.kuzin.tm.repository.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.kuzin.tm.model.AbstractModel;

@NoRepositoryBean
public interface IModelRepository<M extends AbstractModel> extends JpaRepository<M, String> {
}