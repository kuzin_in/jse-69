package ru.kuzin.tm.repository.model;

import org.springframework.stereotype.Repository;
import ru.kuzin.tm.model.Project;

@Repository
public interface IProjectRepository extends IUserOwnedRepository<Project> {
}
